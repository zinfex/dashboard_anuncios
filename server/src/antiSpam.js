import rateLimit from 'express-rate-limit';

const onLimitReached = (req, res, options) => {
    // Se o limite de requisições for excedido, espere 5 segundos
    setTimeout(() => {
      options.next();
    }, 5000);
};
  
const limiter = rateLimit({
    windowMs: 1000,
    max: 3, // 3 requisições a cada 1 segundo
    handler: (req, res, next) => {
      // Chamando a função quando o limite é atingido
      onLimitReached(req, res, { next });
    },
});

export default limiter;
