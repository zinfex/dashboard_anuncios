import * as dotenv from "dotenv";

dotenv.config();

const Autorization = (req, res, next) => {
    const token = req.headers['token'];

    if (!token) {
        return res.status(401).json({ message: 'Token de autenticação não fornecido' }); 
    }

    try {
        if (token == process.env.SECRET) {
            next()
        } else {
            res.status(500).json({
                statusCode: 500,
                message: 'Token inválido'
            });
        }
    } catch (error) {
        console.log(error);

        res.status(500).json({
            statusCode: 500,
            message: 'Erro ao verificar token'
        });
    }
}

export default Autorization;
