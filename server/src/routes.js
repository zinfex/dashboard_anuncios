import express from 'express';
import * as dotenv from "dotenv";

import rotas_anuncios from './anuncios/rotas_anuncios.js';

dotenv.config();

const routes = express.Router();

routes.use(rotas_anuncios)

routes.get('/', async (req, res) => {
    res.send('Acesse /docs para documentação');
})

export default routes;