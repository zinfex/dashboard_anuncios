import express from 'express';
import cors from 'cors';

import * as dotenv from "dotenv";
import routes from './routes.js';
import swaggerUI from 'swagger-ui-express';
import swaggerDocument from '../swagger.json' assert { type: 'json' };

dotenv.config();

const app = express();
app.use(cors());
app.use(express.json());
app.use(routes)

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.use((req, res, next) => {
    if (req.method === "POST") {
        return res.status(405).send('Método não aprovado');
    }
    next();
});

app.listen(process.env.PORT, () => {
    console.log('API online');
});