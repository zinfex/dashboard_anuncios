import limiter from '../antiSpam.js';
import Autorization from '../Auth.js';
import express from 'express';
import * as dotenv from "dotenv";
// import dados from './dados.js'
import dados from './dados.json' assert { type: 'json' };

dotenv.config();

const rotas_anuncios = express.Router()

rotas_anuncios.get('/usuario', limiter, Autorization, async (req, res) => {
    try {
        res.json(dados[0][0].name);
    } catch (error) {
        res.status(500).json(error);
    }
})

rotas_anuncios.get('/fotousuario', limiter, Autorization, async (req, res) => {
    try {
        res.json(dados[0][1].profile_image_url);
    } catch(error) {
        res.status(500).json(error);
    }
})

rotas_anuncios.get('/seguidoresinstagram', limiter, Autorization, async (req, res) => {
    try {
        res.json(dados[0][2].followers_count);
    } catch(error) {
        res.status(500).json(error);
    }
})

rotas_anuncios.get('/anuncios', limiter, Autorization, async (req, res) => {
    try {
        res.json(dados[1]);
    } catch(error) {
        res.status(500).json(error);
    }
});

console.log(dados)

export default rotas_anuncios;