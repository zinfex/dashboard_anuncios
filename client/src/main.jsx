import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import { AnuncioProvider } from './contexts/AnuncioContext.jsx'
import { GraficosProvider } from './contexts/GraficosContext.jsx'

document.documentElement.style.scrollBehavior = 'smooth';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <AnuncioProvider>
      <GraficosProvider>
        <App />
      </GraficosProvider>
    </AnuncioProvider>
  </React.StrictMode>,
)
