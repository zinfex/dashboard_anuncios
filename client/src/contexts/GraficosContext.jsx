import React, { createContext, useState, useContext } from 'react';

const GraficosContext = createContext();

export function GraficosProvider({ children }) {
  
  const [impressoesDatas, setImpressoesDatas] = useState([])
  const [cliquesDatas, setCliquesDatas] = useState([])
  const [cpmDatas, setCpmDatas] = useState([])
  const [ctrDatas, setCtrDatas] = useState([])
  const [cpcDatas, setCpcDatas] = useState([])

  return (
    <GraficosContext.Provider value={{ 
      impressoesDatas, setImpressoesDatas,
      cliquesDatas, setCliquesDatas,
      cpmDatas, setCpmDatas,
      ctrDatas, setCtrDatas,
      cpcDatas, setCpcDatas,
    }}>
      {children}
    </GraficosContext.Provider>
  );
}

export function useGraficos() {
  return useContext(GraficosContext);
}