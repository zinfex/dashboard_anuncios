import React, { createContext, useState, useContext, useEffect } from "react";
import api from "../../config/Api";

const AnuncioContext = createContext();

export function AnuncioProvider({ children }) {
  const [id, setId] = useState(1);
  const [anuncio, setAnuncio] = useState("Selecione um anúncio");
  const [datasAnuncio, setDatasAnuncio] = useState([]);
  const [itemsAnuncio, setItemsAnuncio] = useState([]);
  const [dataEscolhida, setDataEscolhida] = useState([]);

  const [isVisible, setIsVisible] = useState(false);

  const [dadosApi, setDadosApi] = useState([]);

  async function fetch() {
    try {
      const response = await api.get("/anuncios");
      setDadosApi(response.data);
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(() => {
    fetch();
  }, [anuncio]);

  return (
    <AnuncioContext.Provider
      value={{
        id,
        setId,
        anuncio,
        setAnuncio,
        datasAnuncio,
        setDatasAnuncio,
        itemsAnuncio,
        setItemsAnuncio,
        dataEscolhida,
        setDataEscolhida,

        isVisible,
        setIsVisible,

        dadosApi,
        setDadosApi,
      }}
    >
      {children}
    </AnuncioContext.Provider>
  );
}

export function useAnuncio() {
  return useContext(AnuncioContext);
}
