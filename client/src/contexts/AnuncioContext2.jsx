import React, { createContext, useState, useContext, useEffect } from "react";
import api from "../../config/Api";

const AnuncioContext = createContext();

export function AnuncioProvider({ children }) {
  const [anuncioData, setAnuncioData] = useState({
    id: 1,
    anuncio: "Selecione um anúncio",
    datasAnuncio: [],
    itemsAnuncio: [],
    dataEscolhida: [],
    isVisible: false,
    dadosApi: [],
  })

  async function fetch() {
    try {
      const response = await api.get("/anuncios");

      setAnuncioData((prevData) => ({
        ...prevData,
      dadosApi: response.data,
      }));
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(() => {
    fetch();
  }, [anuncioData.anuncio]);

  return (
    <AnuncioContext.Provider
      value={{ anuncioData, setAnuncioData }}
    >
      {children}
    </AnuncioContext.Provider>
  );
}

export function useAnuncio() {
  return useContext(AnuncioContext);
}
