import './App.css'
import React from 'react';
import { Button, Grid, Typography, Menu, MenuItem } from '@mui/material';
import Block1 from './components/Block1';
import Block2 from './components/Block2';
import Block3 from './components/Block3';
import Block4 from './components/Block4';
import Block5 from './components/Block5';
import logo from './assets/logo085.png'
import metalogo from './assets/metalogo.png'
import PageHeader from './components/PageHeader';

export default function App() {

  return (
    <>
     <header id='header'>
        <img src={logo} alt='logo' style={{width: 100}}/>
        <img src={metalogo} alt='logo' style={{width: 80}}/>
     </header>

     <PageHeader />

    <Block5 />

    <Block4 />

    <Grid
      sx={{
        px: 4
      }}
      container
      direction="row"
      justifyContent="center"
      alignItems="stretch"
      spacing={4}
    >
      <Grid item xs={12}>
        <Block1 /> 
      </Grid>
      
      <Grid item md={7} xs={12}>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={4}
        >
    
        </Grid>
      </Grid>
      <Grid item md={6} xs={12} className='graficos' >
        <Block2 />
      </Grid>
      <Grid item md={6} xs={12} className='graficos' >
        <Block3 />
      </Grid>
      
      {/* <Grid item lg={6} md={6} xs={12} className='grafico-pizza'>
        <SalesByCategory />
      </Grid>*/}
    </Grid>

    <footer>
    <p>© 2024 - Todos os direitos reservados a 085 Digital</p> <p>Crafted by 085.digital</p>
    </footer>
    </>
  )
}