import { Fragment, useState, useEffect } from "react";

import {
  Box,
  Card,
  Typography,
  CircularProgress,
  Button,
  ListItemAvatar,
  ListItemText,
  Divider,
  List,
  ListItem,
  Avatar,
  circularProgressClasses,
  styled,
  useTheme,
  LinearProgress,
  MenuItem,
  Menu
} from "@mui/material";

import Chart from "react-apexcharts";

import Scrollbar from "./ScrollBar";
import FiberManualRecordTwoToneIcon from "@mui/icons-material/FiberManualRecordTwoTone";
import KeyboardArrowDownTwoToneIcon from "@mui/icons-material/KeyboardArrowDownTwoTone";
import KeyboardArrowUpTwoToneIcon from "@mui/icons-material/KeyboardArrowUpTwoTone";
import ArrowForwardTwoToneIcon from "@mui/icons-material/ArrowForwardTwoTone";
import { useAnuncio } from "../contexts/AnuncioContext";

const ListWrapper = styled(List)(
  () => `
    .MuiDivider-root:first-of-type {
        display: none;
    }
  `
);

export default function Block4() {
  const [nomes, setNomes] = useState([]);
  const [datas, setDatas] = useState([]);
  const [removeLoading, setRemoveLoading] = useState(false)

  const { setIsVisible, dadosApi } = useAnuncio();

  const {
    anuncio,
    setAnuncio,
    setId,
    datasAnuncio,
    setDatasAnuncio,
    setDataEscolhida,
    dataEscolhida,
  } = useAnuncio();

  const fetchData = async () => {
    try {
      if (dadosApi.length === 0) {
        setRemoveLoading(false)
        return;
      }

      setNomes(dadosApi.map(objeto => objeto.ad_name));
      setDatas(dadosApi.map(objeto => objeto.date_start));

      setRemoveLoading(true)
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    fetchData();
  }, [dadosApi]); 

  useEffect(() => {
    verifyDatas();
  }, [anuncio]);

  useEffect(() => {
    setDataEscolhida(datasAnuncio[datasAnuncio.length - 1]);
  }, [datasAnuncio]);

  function handleButton(nome, id) {
    fetchData()
    setAnuncio(nome);
    setId(id - 1);
    verifyDatas();
    setDataEscolhida(datasAnuncio[datasAnuncio.length - 1]);

    setIsVisible(true);

    var div = document.getElementById('menuInicio');
    div.style.display = 'none';
  }

  const items = [];

  for (let i = 1; i < nomes.length; i++) {
    const newItem = {
      id: i + 1,
      jobtitle: nomes[i],
      data: datas[i],
      progress: 0,
      arrow: "",
      value: "",
    };

    items.push(newItem);
  }

  const datasPorNome = {};

  // Preencher o objeto com as datas correspondentes a cada nome
  items.forEach(({ jobtitle, data }) => {
    if (!datasPorNome[jobtitle]) {
      datasPorNome[jobtitle] = [];
    }
    datasPorNome[jobtitle].push(data);
  });

  function verifyDatas() {
    // Verifica se o nome estÃ¡ presente no objeto datasPorNome
    if (datasPorNome.hasOwnProperty(anuncio)) {
      setDatasAnuncio(datasPorNome[anuncio]);
    } else {
      setDatasAnuncio([]); // Define como array vazio caso o nome nÃ£o seja encontrado
    }
  }
  
  return (
    <>
      <Card
        id="menuInicio"
        sx={{
            backgroundColor: 'transparent', 
            borderRadius: 5,
            position: "relative",
            color: "white",
            boxShadow: '1px 1px 10px 2px rgba(0, 0, 0, 0.617)',
            marginInline: 30,
            
        }}
      >
        <Typography
          fontWeight="bold"
          sx={{
            py: 1,
            px: 2,
            fontSize: 25,
            fontWeight: '700',
            fontFamily: "Poppins",
            borderBottom: "1px solid rgba(255, 255, 255, 0.126);"
          }}
          component="h6"
          variant="caption"
        >
          {"Anúncios Disponíveis"}
        </Typography>

        <Box
          sx={{
            height: 300,
          }}
        >
          <Scrollbar>
            <ListWrapper disablePadding>

              {items.map((item, index) => {
                // Verifica se o jobtitle jÃ¡ existe em um item anterior da lista
                const jobtitleExists = items
                  .slice(0, index)
                  .some((prevItem) => prevItem.jobtitle === item.jobtitle);

                // Se o jobtitle jÃ¡ existe, nÃ£o renderiza o item
                if (jobtitleExists) {
                  return null;
                }

                return (
                  <Fragment key={item.id}>
                    <Divider />
                    <ListItem
                      sx={{
                        py: 2,
                        px: 2.5,
                      }}
                    >
                      <ListItemAvatar
                        sx={{
                          display: "flex",
                          mr: 0.5,
                          fontWeight: '700',
                          fontFamily: "Poppins",
                        }}
                      >
                        <Button
                          endIcon={<ArrowForwardTwoToneIcon />}
                          size="small"
                          onClick={() => handleButton(item.jobtitle, item.id)}
                          className="botao"
                          sx={{fontFamily: 'Poppins', fontWeight: '700'}}
                        >
                          {item.jobtitle}
                          
                        </Button>
                      </ListItemAvatar>
                    </ListItem>
                  </Fragment>
                  )              
              })}
            </ListWrapper>
          </Scrollbar>
        </Box>
        <Divider />
      </Card>
    </>
  );
}