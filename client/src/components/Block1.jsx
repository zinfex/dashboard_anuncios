import { useState, useEffect } from 'react';
import { useAnuncio } from '../contexts/AnuncioContext';
import { useGraficos } from '../contexts/GraficosContext';
import api from '../../config/Api';
import {
  Menu,
  Typography,
  Box,
  Avatar,
  Card,
  Grid,
  styled,
} from '@mui/material';

import {
  ArrowDownwardTwoTone as ArrowDownwardIcon,
  ArrowUpwardTwoTone as ArrowUpwardIcon,
  RemoveRedEye as RemoveRedEyeIcon,
  LocalOffer as LocalOfferIcon,
  Reviews as ReviewsIcon,
  CurrencyExchange as CurrencyExchangeIcon,
  MoneyOff as MoneyOffIcon,
} from '@mui/icons-material';

import CountUp from 'react-countup';
import MouseIcon from '@mui/icons-material/Mouse';
import InstagramIcon from '@mui/icons-material/Instagram';

const AvatarWrapper = styled(Avatar)(({ theme }) => `
  color: #FFFF;
  background-color: black;
  width: ${theme.spacing(5.5)};
  height: ${theme.spacing(5.5)};
`);

export default function Block1() {
  const { dataEscolhida, anuncio, datasAnuncio, isVisible, dadosApi } = useAnuncio();
  const {
    impressoesDatas, setImpressoesDatas,
    cliquesDatas, setCliquesDatas,
    cpmDatas, setCpmDatas,
    ctrDatas, setCtrDatas,
    cpcDatas, setCpcDatas,
  } = useGraficos();

  const [ctr, setCtr] = useState(0);
  const [cpc, setCpc] = useState(0);
  const [cpm, setCpm] = useState(0);
  const [impressoes, setImpressoes] = useState(0);
  const [cliques, setCliques] = useState(0);
  const [valorGasto, setValorGasto] = useState(0);
  const [seguidores, setSeguidores] = useState(0);

  useEffect(() => {
    fetchData();
  }, [anuncio, dataEscolhida, datasAnuncio, dadosApi, setImpressoesDatas]);

  function ComparacoesInvertido(arrayDados, indiceSelecionado) {
    if (indiceSelecionado <= 0 || indiceSelecionado >= arrayDados.length) {
      return false; // Índice inválido, retornar false
    }
    return arrayDados[indiceSelecionado] < arrayDados[indiceSelecionado - 1];
  }

  function Comparacoes(arrayDados, indiceSelecionado) {
    // if (indiceSelecionado < 0 || indiceSelecionado > arrayDados.length) {
    //   return false; // Verificação
    // }

    console.log(arrayDados, indiceSelecionado)
    return arrayDados[indiceSelecionado] > arrayDados[indiceSelecionado - 1];
  }

  async function fetchData() {
    try {
      const dadosDataEscolhida = dadosApi.filter(item => item.ad_name === anuncio && item.date_start === dataEscolhida);
      // console.log(dadosDataEscolhida)

      setCtr(dadosDataEscolhida[0]?.ctr || 0);
      setCpc(dadosDataEscolhida[0]?.cpc || 0);
      setCpm(dadosDataEscolhida[0]?.cpm || 0);
      setImpressoes(dadosDataEscolhida[0]?.impressions || 0);
      setCliques(dadosDataEscolhida[0]?.clicks || 0);
      setValorGasto(dadosDataEscolhida[0]?.spend || 0);

      const response = await api.get("/seguidoresinstagram");
      setSeguidores(response.data);
      updateDatas(dadosApi);
    } catch (error) {
      console.log(error);
    }
  }

  function updateDatas(data) {
    const fillArray = (array, data, prop) => {
      return datasAnuncio.map(date => {
        const filtered = data.filter(item => item.ad_name === anuncio && item.date_start === date);
        return filtered.length > 0 ? filtered[0][prop] : 0;
      });
    };

    setImpressoesDatas(fillArray(impressoesDatas, data, 'impressions'));
    setCliquesDatas(fillArray(cliquesDatas, data, 'clicks'));
    setCpmDatas(fillArray(cpmDatas, data, 'cpm'));
    setCtrDatas(fillArray(ctrDatas, data, 'ctr'));
    setCpcDatas(fillArray(cpcDatas, data, 'cpc'));
  }

  const renderCard = (title, value, icon, color, compareData, format) => (
    <Grid item xs={12} sm={6} lg={3}>
      <Card sx={{ px: 3, pb: 3, pt: 3, color: 'white', backgroundColor: 'transparent', border: `1px solid rgba(255, 255, 255, 0.126)`, borderRadius: 3, boxShadow: 'rgba(0, 0, 0, 0.17) 0px -23px 25px 0px inset, rgba(0, 0, 0, 0.15) 0px -36px 30px 0px inset, rgba(0, 0, 0, 0.1) 0px -79px 40px 0px inset, rgba(0, 0, 0, 0.06) 0px 2px 1px, rgba(0, 0, 0, 0.09) 0px 4px 2px, rgba(0, 0, 0, 0.09) 0px 8px 4px, rgba(0, 0, 0, 0.09) 0px 16px 8px, rgba(0, 0, 0, 0.09) 0px 32px 16px;' }}>
        <Box display="flex" alignItems="center">
          <AvatarWrapper style={{ backgroundColor: color }}>
            {icon}
          </AvatarWrapper>
          <Typography sx={{ ml: 1.5, fontSize: 25, fontWeight: '700', fontFamily: "Poppins" }} variant="subtitle2" component="div">
            {title}
          </Typography>
        </Box>
        <Box display="flex" alignItems="center" sx={{ ml: -2, pt: 2, justifyContent: 'center' }}>
          {compareData && (compareData(value) ? <ArrowUpwardIcon sx={{ color: 'green' }} /> : <ArrowDownwardIcon sx={{ color: 'red' }} />)}
          <Typography sx={{ pl: 1, fontSize: 35, fontWeight: '700', fontFamily: "Poppins" }} variant="h1">
            <CountUp start={0} end={value} duration={3} separator="" delay={3} {...format} />
          </Typography>
        </Box>
      </Card>
    </Grid>
  );

  return isVisible && (
    <Grid container spacing={2}>
      {renderCard('Cliques', cliques, <MouseIcon fontSize="small" />, '#384E75', (value) => Comparacoes(cliquesDatas, cliquesDatas.indexOf(value)), { decimals: 0, decimal: '.', prefix: '' })}
      {renderCard('Impressões', impressoes, <RemoveRedEyeIcon fontSize="small" />, '#384E75', (value) => Comparacoes(impressoesDatas, impressoesDatas.indexOf(value)), { decimals: 0, decimal: '.', prefix: '' })}
      {renderCard('CPM', cpm, <LocalOfferIcon fontSize="small" />, '#1D2AFF', (value) => Comparacoes(cpmDatas, cpmDatas.indexOf(value)), { decimals: 3, decimal: ',', prefix: '' })}
      {renderCard('CTR', ctr, <ReviewsIcon fontSize="small" />, '#52C824', (value) => Comparacoes(ctrDatas, ctrDatas.indexOf(value)), { decimals: 3, decimal: ',', prefix: '' })}
      {renderCard('CPC', cpc, <CurrencyExchangeIcon fontSize="small" />, '#FCA114', (value) => Comparacoes(cpcDatas, cpcDatas.indexOf(value)), { decimals: 2, decimal: ',', prefix: '' })}
      {renderCard('Valor usado', valorGasto, <MoneyOffIcon fontSize="small" />, '#FF1A4A', null, { decimals: 2, decimal: ',', prefix: 'R$' })}
      {renderCard('Seguidores', seguidores, <InstagramIcon fontSize="small" />, '#384E75', null)}
    </Grid>
  );
}
