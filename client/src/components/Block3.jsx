import {
    Divider,
    Box,
    Card,
    Typography,
    useTheme,
  } from '@mui/material';
  
  import CountUp from 'react-countup';
  import Chart from 'react-apexcharts'
  import { useAnuncio } from '../contexts/AnuncioContext';
  import { useEffect, useState } from 'react';
  
  export default function Block3() {
    const theme = useTheme();

    const { 
      anuncio, 
      dataEscolhida, 
      datasAnuncio, 
      setImpressoesDatas,
      isVisible,
      dadosApi
    } = useAnuncio();

    const [valoresGastos, setValoresGastos] = useState([])
    const [totValoresGastos, setTotValoresGastos] = useState([])
    
    async function fetch() {
      const dadosValorGasto = [];
      
      datasAnuncio.forEach(async (data) => {
        const dadosDataEscolhida = dadosApi.filter(item => item.ad_name === anuncio && item.date_start === data);
  
        if (dadosDataEscolhida.length > 0) {
          dadosValorGasto.push(dadosDataEscolhida[0].spend);
        } else {
          dadosValorGasto.push(0);
        }
      });

      setValoresGastos(dadosValorGasto)
    }
    
    useEffect(() => {
      fetch()
    }, [dataEscolhida, datasAnuncio, setImpressoesDatas, anuncio])

    useEffect(() => {
      setTotValoresGastos(valoresGastos.reduce((accumulator, currentValue) => accumulator + Number(currentValue), 0));
    }, [fetch])
    
    const options = {
    chart: {
      type: "bar",
      height: 350,
      toolbar: {
        show: true,
      },
      zoom: {
        enabled: true,
      },
    },
    colors: ['#FCA114'],
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: "55%",
        endingShape: "rounded",
      },
    },
    dataLabels: {
      enabled: true,
    },
    xaxis: {
      //DATAS DO ANÚNIO
      type: 'datetime',
      categories: datasAnuncio,
    },
    fill: {
      opacity: 1,
    },
    tooltip: {
      y: {
        formatter: function (val) {
          return val 
        },tooltip: {
      x: {
        format: "dd-MM-yyyy",
      },
      fixed: {
        enabled: false,
        position: 'topRight'
      }
    },
      },
    },
  };

    const series = [
      {
        name: 'Valor usado',
        data: valoresGastos
      }
    ];

    return (
      <>
      {isVisible && (
      <Card sx={{
            backgroundColor: 'transparent', 
            borderRadius: 5,
            boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px;'
      }}>
        <Box
          px={2}
          py={1.8}
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          color="white"
          borderBottom="1px solid rgba(255, 255, 255, 0.126);"
        >
          <Box>
            <Typography
              gutterBottom
              sx={{
                fontSize: 24,
                fontWeight: '700',
                fontFamily: "Poppins",
              }}
              variant="h4"
            >
              Valores Usados
            </Typography>
            <Typography variant="subtitle2" sx={{fontWeight: '700', fontFamily: "Poppins",}}>
              Exibição por data de valores gastos com o anúncio
            </Typography>
          </Box>
        </Box>
       
        <Box px={5}>
          <p style={{marginBottom: 0, color: '#FCA114', backgroundColor: '#384E75', width: 'fit-content', padding: 5, borderRadius: 5}}>Total Usado</p>
          <Typography
            color="white"
            component="h3"
            fontWeight="bold"
            sx={{
              mb: 5,
              fontSize: 50,
              color: '#FCA114'
            }}
          >
            <CountUp
              start={0}
              end={totValoresGastos}
              duration={4}
              separator=""
              delay={3}
              decimals={2}
              decimal=","
              prefix="R$"
              suffix=""
            />
          </Typography>
  
        </Box>
        <Chart options={options} series={series} type="bar" height={260}/>
      </Card>
      )}
      </>
    );
  }
  