import { useRef, useState, useEffect } from 'react';
import {
  Typography,
  Button,
  Box,
  Avatar,
  MenuItem,
  Menu,
  styled
} from '@mui/material';
import KeyboardArrowDownTwoToneIcon from '@mui/icons-material/KeyboardArrowDownTwoTone';
import { useAnuncio } from '../contexts/AnuncioContext';
import api from '../../config/Api';
import LinearProgress from '@mui/material/LinearProgress';
import { format } from 'date-fns';

const AvatarPageTitle = styled(Avatar)(
  ({ theme }) => `
      width: ${theme.spacing(8)};
      height: ${theme.spacing(8)};
      margin-right: ${theme.spacing(2)};
`
);

function PageHeader() {
  const { datasAnuncio, dataEscolhida, setDataEscolhida } = useAnuncio();
  const [dataAnuncio, setDataAnuncio] = useState([])
  const [nomeUser, setNomeUser] = useState()
  const [fotoUsuario, setFotoUsuario] = useState()

  const [openPeriod, setOpenMenuPeriod] = useState(false);
  const actionRef1 = useRef(null);
  
  useEffect(() => {
    setDataEscolhida(dataAnuncio)
    fetch()
  }, [])

  async function fetch() {
    try {
      const response = await api.get('/usuario')
      setNomeUser(response.data.name)
      const responseFoto = await api.get("/fotousuario")
      setFotoUsuario(responseFoto.data)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <Box
      display="flex"
      alignItems={{ xs: 'stretch', md: 'center' }}
      flexDirection={{ xs: 'column', md: 'row' }}
      justifyContent="space-between"
    >
      <Box display="flex" alignItems="center" p={5}>
        <AvatarPageTitle variant="rounded" sx={{backgroundColor: '#384E75'}}><img src={fotoUsuario} alt='logo' style={{width: 70}}/></AvatarPageTitle>
        <Box>
          <Typography variant="h4" component="h3" sx={{
            fontWeight: '700',
            fontFamily: "Poppins",
          }}>

            {nomeUser}
            
          </Typography>
        </Box>
      </Box>
      <Box>
        <Button
          variant="outlined"
          ref={actionRef1}
          onClick={() => setOpenMenuPeriod(true)}
          sx={{
            mr: 15,
            fontSize: 20,
            fontWeight: '700',
            fontFamily: "Poppins",
            background: 'transparent',
          }}
          endIcon={<KeyboardArrowDownTwoToneIcon fontSize="small" />}
        >
          {dataEscolhida}
        </Button>
        
        <Menu
          disableScrollLock
          anchorEl={actionRef1.current}
          onClose={() => setOpenMenuPeriod(false)}
          // sx={{
          //   backdropFilter: 'blur(2px)',
          //   WebkitBackdropFilter: 'blur(2px)',
          // }}
          open={openPeriod}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
        >
          {datasAnuncio.map((dataAnuncio) => (
            <MenuItem style={{fontSize: 20, color: '#1976D2',}} onClick={() => setDataEscolhida(dataAnuncio)}>
              {dataAnuncio}
            </MenuItem>
          ))}
        </Menu>
      </Box>

      
    </Box>
  );
}

export default PageHeader;