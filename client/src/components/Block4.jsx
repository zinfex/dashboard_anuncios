import { Fragment, useState, useEffect } from "react";
import React from "react";

import {
  Box,
  Card,
  Typography,
  CircularProgress,
  Button,
  ListItemAvatar,
  ListItemText,
  Divider,
  List,
  ListItem,
  Avatar,
  circularProgressClasses,
  styled,
  useTheme,
  LinearProgress,
  MenuItem,
  Menu
} from "@mui/material";

import Chart from "react-apexcharts";

import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowForwardTwoToneIcon from "@mui/icons-material/ArrowForwardTwoTone";
import { useAnuncio } from "../contexts/AnuncioContext";

const ListWrapper = styled(List)(
  () => `
    .MuiDivider-root:first-of-type {
        display: none;
    }
  `
);

export default function Block4() {
  const [nomes, setNomes] = useState([]);
  const [datas, setDatas] = useState([]);
  const [removeLoading, setRemoveLoading] = useState(false)

  const { setIsVisible, dadosApi } = useAnuncio();

  const {
    anuncio,
    setAnuncio,
    setId,
    datasAnuncio,
    setDatasAnuncio,
    setDataEscolhida,
    dataEscolhida,
    isVisible
  } = useAnuncio();

  const fetchData = async () => {
    try {
      if (dadosApi.length === 0) {
        setRemoveLoading(false)
        return;
      }

      setNomes(dadosApi.map(objeto => objeto.ad_name));
      setDatas(dadosApi.map(objeto => objeto.date_start));

      setRemoveLoading(true)
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    fetchData();
  }, [dadosApi]); 

  useEffect(() => {
    verifyDatas();
  }, [anuncio]);

  useEffect(() => {
    setDataEscolhida(datasAnuncio[datasAnuncio.length - 1]);
  }, [datasAnuncio]);

  function handleButton(nome, id) {
    handleClose()
    fetchData()
    setAnuncio(nome);
    setId(id - 1);
    verifyDatas();
    setDataEscolhida(datasAnuncio[datasAnuncio.length - 1]);

    setIsVisible(true);
    window.location.href = "#header";
  }

  const items = [];

  for (let i = 1; i < nomes.length; i++) {
    const newItem = {
      id: i + 1,
      jobtitle: nomes[i],
      data: datas[i],
      progress: 0,
      arrow: "",
      value: "",
    };

    items.push(newItem);
  }

  const datasPorNome = {};

  // Preencher o objeto com as datas correspondentes a cada nome
  items.forEach(({ jobtitle, data }) => {
    if (!datasPorNome[jobtitle]) {
      datasPorNome[jobtitle] = [];
    }
    datasPorNome[jobtitle].push(data);
  });

  function verifyDatas() {
    // Verifica se o nome está presente no objeto datasPorNome
    if (datasPorNome.hasOwnProperty(anuncio)) {
      setDatasAnuncio(datasPorNome[anuncio]);
    } else {
      setDatasAnuncio([]); // Define como array vazio caso o nome não seja encontrado
    }
  }

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  return (
    <>
    {isVisible && (
      <Typography style={{marginLeft: 40, marginBottom: 30}}>
      <Button
      sx={{
        fontSize: 20,
        fontWeight: '700',
        fontFamily: "Poppins",
      }}
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        {anuncio} <ArrowDownwardIcon/>
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
            {items.map((item, index) => {
                // Verifica se o jobtitle já existe em um item anterior da lista
                const jobtitleExists = items
                  .slice(0, index)
                  .some((prevItem) => prevItem.jobtitle === item.jobtitle);

                // Se o jobtitle já existe, não renderiza o item
                if (jobtitleExists) {
                  return null;
                }

                return (
                  <Fragment key={item.id}>
                    <Divider />
                    <ListItem
                      sx={{
                      
                        px: 2.5,
                      }}
                    >
                      <ListItemAvatar
                        sx={{
                          display: "flex",
                          mr: 0.5,
                          fontWeight: '700',
                          fontFamily: "Poppins",
                        }}
                      >
                        <Button
                          endIcon={<ArrowForwardTwoToneIcon />}
                          size="small"
                          onClick={() => handleButton(item.jobtitle, item.id)}
                          className="botao"
                          sx={{fontFamily: 'Poppins', fontWeight: '700'}}
                        >
                          {item.jobtitle}
                          
                        </Button>
                      </ListItemAvatar>
                    </ListItem>
                  </Fragment>
                  )              
              })}
      </Menu>
    </Typography>
    )}</>
  );
}