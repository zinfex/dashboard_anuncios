import { Box, Card, Typography, Divider, Grid, useTheme, Button } from "@mui/material";

import { useAnuncio } from "../contexts/AnuncioContext";

import Chart from "react-apexcharts";
import { useGraficos } from "../contexts/GraficosContext";


export default function Block2() {
  
  const { datasAnuncio, isVisible } = useAnuncio();
  const { impressoesDatas } = useGraficos()

  const options = {
    chart: {
      type: 'area',
      height: 350,
      stacked: true,
    },

    dataLabels: {
      enabled: true
    },
    stroke: {
      curve: 'smooth'
    },
    
    xaxis: {
      type: 'datetime',
      axisBorder: {
        show: false
      },
      axisTicks: {
        show: false
      },
      categories: datasAnuncio
    },
    yaxis: {
      tickAmount: 4,
      floating: false,
    
      labels: {
        offsetY: -7,
        offsetX: 0,
      },
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false
      }
    },
    fill: {
      opacity: 0.5
    },
    tooltip: {
      x: {
        format: "dd-MM-yyyy",
      },
      fixed: {
        enabled: false,
        position: 'topRight'
      }
    },
    grid: {
      yaxis: {
        lines: {
          offsetX: -30
        }
      },
      padding: {
        left: 20
      }
    }
  }

  const dadosChart = [
    {
      name: "Impressões",
      data: impressoesDatas,
    },
  ];

  return (
    <>
    {isVisible && (
    <Card sx={{
      backgroundColor: 'transparent', 
      borderRadius: 5,
      boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px;'
    }}>
      <Box
        display="flex"
        alignItems="center"
        p={2}
        justifyContent="space-between"
        color="white"
        borderBottom="1px solid rgba(255, 255, 255, 0.126);"
      >
        <Box>
          <Typography
            component="div"
            sx={{
              fontSize: 24,
              fontWeight: '700',
              fontFamily: "Poppins",
            }}
            gutterBottom
            variant="h3"
          >
            {'Impressões'}
          </Typography>
          <Typography
            component="div"
            fontWeight="normal"
            
            variant="h5"
            
            sx={{
              fontSize: 15,
              fontWeight: '700',
              fontFamily: "Poppins",
            }}
          >
            Selecione a opção que você deseja ver o gráfico
          </Typography>
        </Box>
      </Box>

      <Box p={2}>
        <Grid item xs={12} md={12}>
          <Chart       
            options={options}
            series={dadosChart}
            type="area"
            height={390}
          />
        </Grid>
      </Box>
      <Divider />
    </Card> )}
    </>
  );
}